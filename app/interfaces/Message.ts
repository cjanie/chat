import { Person } from "./Person";

export interface Message {
    person: Person
    text: string
}