import React, { useLayoutEffect, useState } from "react";
import { View, Text, TextInput, Button, StyleSheet } from "react-native";
import { Message } from "../interfaces/Message";
import chatStore from "../store/chat";
import styles from "../styles/styles";

/**
 * Component PersonTwo as a function
 */
const PersonTwo = () => {

    const [chatState, setChatState] = useState(chatStore.initialState);

    useLayoutEffect(() => {
        chatStore.subscribe(setChatState);
        chatStore.init();
    }, []);

    const [value, onChangeText] = React.useState('');
    
    return (
        <View style={styles.container}>
            <Text style={styles.title_text}>Cortana</Text>
            <View style={styles.chat_box}>
                {chatState.data.map((message: Message) => (
                    /**
                     * Warning: Each child in a list should have a unique "key" prop
                     * https://fb.me/react-warning-keys 
                     */
                    <View style={message.person.style[0]}>
                        <Text style={[styles.message_person_text, message.person.style]}>{message.text}</Text>
                        <View style={styles.clear}></View>
                    </View>
                ))}
            </View>
            <View style={styles.message_form_container}>
                <TextInput 
                    style={styles.input_text}
                    placeholder='Type here...'
                    onChangeText={text => onChangeText(text)}
                    value={value}
                />
                <Button
                    title='Send'
                    onPress={() => {
                        if(value.length > 0) {
                            chatStore.sendMessage({
                                person: {
                                    name: 'person-two',
                                    style: [styles.message_person_two, styles.message_person_two_text]
                                },
                                text: value
                            });
                            onChangeText('');
                        }
                    }}
                />
            </View>
            
            <Button
                title='Clear chat'
                onPress={() => {chatStore.clearChat()}}
            />
            
        </View>
    );
}

export default PersonTwo