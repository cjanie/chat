import React, { useEffect, useState } from "react"
import { View, Text } from "react-native";
import { useLocation, Link } from "react-router-dom";
import { RoutePath } from "../enums/routePath";
import chatStore from "../store/chat"
import styles from "../styles/styles";

const PersonSwitcher = () => {

    const [chatState, setChatState] = useState(chatStore.initialState);

    const location = useLocation().pathname.split('/') [1]; // location is the route name

    useEffect(() => {
        chatStore.subscribe(setChatState);
        chatStore.init();
    }, []);

    const messageNotification = chatState.newDataCount > 0
    && (<Text style={styles.notify}>{chatState.newDataCount}</Text>);

    return (
        <View>
            <View style={styles.switcher_container}>
                <View style={styles.switcher}>
                    <Link to={RoutePath.personOne}>
                        <View>
                            <Text>Person One</Text> 
                        </View>    
                    </Link>
                    {location !== RoutePath.personOne.substring(1) 
                        && location.length > 1 
                        && messageNotification
                    }
                </View>
                
                <View style={styles.switcher}>
                    <Link to={RoutePath.personTwo}>
                        <View>
                            <Text>Person Two</Text>
                        </View>
                    </Link>
                    {location !== RoutePath.personTwo.substring(1) 
                        && messageNotification
                    }            
                </View>    
            </View>
        </View>
    );
}

export default PersonSwitcher