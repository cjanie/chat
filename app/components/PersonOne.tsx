// https://blog.logrocket.com/rxjs-with-react-hooks-for-state-management/

import React, {useLayoutEffect, useState} from 'react'
import { Text, View, TextInput, Button, StyleSheet } from 'react-native';
import { PersonSwitcher } from '.';
import { Message } from '../interfaces/Message';
import chatStore from '../store/chat';
import styles from '../styles/styles';


/**
 * Component PersonOne as a function
 */
const PersonOne = () => {

    const [chatState, setChatState] = useState(chatStore.initialState);

    useLayoutEffect(() => {
        chatStore.subscribe(setChatState);
        chatStore.init();
    }, []);

    const [value, onChangeText] = React.useState('');

    

    return (
        <View style={styles.container}>
            
            <Text style={styles.title_text}>Mycroft</Text>
            <View style={styles.chat_box}>
                {chatState.data.map((message: Message) => (
                       
                    <View style={message.person.style[0]}>
                        <Text style={[styles.message_person_text, message.person.style]}>{message.text}</Text>
                        <View style={styles.clear}></View>
                    </View>
                ))}
            </View>
            <View style={styles.message_form_container}>
                <TextInput 
                    style={styles.input_text}
                    placeholder='Type here...'
                    onChangeText={text => onChangeText(text)}
                    value={value}
                />
                <Button
                    title='Send'
                    onPress={() => {
                        if(value.length > 0) {
                            chatStore.sendMessage({
                                person: {
                                    name: 'person-one',
                                    style: [styles.message_person_one, styles.message_person_one_text]
                                },
                                text: value
                            });
                            onChangeText('');
                        }
                        
                    }}
                />
            </View>
            
            <Button
                title='Clear chat'
                onPress={() => {chatStore.clearChat()}}
            />
            
        </View>
    );

}

export default PersonOne