/**
 * lib rxjs for reactive programming using Observables
 * to compose asynchronous or callback-based code easier
 * 
 * OBSERVABLE
 * It is a data stream that houses data that can be passed through different threads
 * The application uses an Observable to supply data to the components
 * 
 * OBSERVER
 * It consumes the data supplied by an Observable
 * The application uses a setState Hook to consume data from the observable
 * 
 * SUBSCRIPTION
 * In order that an Observer consumes data of an Observable, subscribe it to an Observable
 * The application uses the subscribe() method to subscribe the setState Observer to the Observable
 * 
 * rxjs Subject is both an Obversable and an Observer so that
 * When a Subject receives any data, that data is forwarded to every Observer subscribed to it
 * 
 * Subscribe the differents React Hooks setState functions to the Subject so that
 * When the subject receives data, it forwards it to every state associated with the setState function
 * Create a subscribe methode for this purpose
 */

import { Subject } from 'rxjs'
import { Message } from '../interfaces/Message'


/**
 * Create the Observable subject
 */
const subject = new Subject()


const initialState = {
    status: '',
    data: [] as Message[],
    newDataCount: 0,
    error: ''
}

let state = initialState


/**
 * Create a store object. 
 * It has methods : init, subscribe, sendMessage, clearChat
 * It has a property which is the initialState
 */
const chatStore = {
    init: () => {
        state = {...state, newDataCount: 0}
        subject.next(state);
    },
    subscribe: (setState: any) => subject.subscribe(setState),
    sendMessage: (message: Message) => {
        state = {
            ...state,
            data: [...state.data, message],
            newDataCount: state.newDataCount + 1
        }
        subject.next(state);
    },
    clearChat: () => {
        state = initialState;
        subject.next(state);
    },
    initialState
}

export default chatStore