import { StyleSheet } from "react-native";

const styles = StyleSheet.create(
    {
        container: {
            flex: 1,
            fontFamily: 'Arial, Helvetica, sans-serif',
            padding: 5,
            backgroundColor:'orange'
        },
        title_text: {
            fontSize: 18,
            padding: 5
        },
        chat_box: {
            flex: 4,
            backgroundColor: '#202020',
            padding: 5,
            borderRadius: 20,
            overflow: 'scroll'
        },
        message_person_one: {
            alignItems: 'flex-start'
        },
        message_person_two: {
            alignItems: 'flex-end'
        },
        message_person_text: {
            color: '#fff',
            minWidth: '20%',
            maxWidth: '60%',
            padding: 10,
            marginVertical:1,
            textAlign: 'center',
            borderRadius: 30
        },
        message_person_one_text: {
            backgroundColor: 'rgb(0, 173, 231)'
        },
        message_person_two_text: {
            backgroundColor: '#06c406',
        },
        clear: {
            flex: 1
        },
        message_form_container: {
            flex: 1,
            textAlign: 'center',
            marginTop: 10
        },
        input_text: {
            height: 40,
            borderColor: 'green',
            borderWidth: 1,
            padding: 5,
            marginVertical: 10,
            shadowRadius: 20,
            shadowColor: 'green',
            backgroundColor: 'white'
        },
        switcher_container: {
            flexDirection: 'row',
            paddingTop: 5,
            justifyContent: "center"
        },
        switcher: {
            width: 100,
            padding: 5,
            margin:15,
            borderRadius: 25,
            shadowRadius: 20,
            shadowColor:'green',
            alignItems:'center'
        },
        notify: {
            position:'absolute',
            backgroundColor:'#db0000',
            color: 'white',
            height: 30,
            width: 30,
            borderRadius: 25,
            padding: 5,
            marginTop: -20,
            marginLeft: 110,
            textAlign: 'center'
        }
    }
)

export default styles