export enum RoutePath {
    default = '/',
    personOne = '/person-one', 
    personTwo = '/person-two'
}