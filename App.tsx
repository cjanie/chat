import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { PersonOne, PersonTwo, PersonSwitcher } from './app/components';
import { RoutePath } from './app/enums/routePath';




export default function App() {
  return (
    
    <BrowserRouter>
      
      <>
        <PersonSwitcher/>
        <Switch>
          <Route path={RoutePath.default} component={PersonOne} exact/>
          <Route path={RoutePath.personOne} component={PersonOne} exact/>
          <Route path={RoutePath.personTwo} component={PersonTwo} exact/>
        </Switch>
      </>
    </BrowserRouter>
  );
}

